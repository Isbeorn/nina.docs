The N.I.N.A. source code itself is licensed under the Mozilla Public License Version 2.0. 
However it depends on various third party libraries which carry their own copyright notices and license terms which are explained below.

| Library              | Project Site       | License    |
| -------------------- |:-----------|:----------|
| Accord                   | [http://accord-framework.net/](http://accord-framework.net/) | [GNU Lesser General Public License version 2.1](https://www.gnu.de/documents/lgpl-2.1.de.html) |
| ASCOM                    | [https://ascom-standards.org/](https://ascom-standards.org/) | [The MIT License](https://opensource.org/licenses/MIT) |
| AsyncEnumerator          | [https://github.com/Dasync/AsyncEnumerable]() | [The MIT License](https://opensource.org/licenses/MIT)                   |
| Castle Core              | [https://github.com/castleproject/Core](https://github.com/castleproject/Core) | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) |
| CommunityToolkit.Mvvm    | [https://github.com/CommunityToolkit/dotnet](https://github.com/CommunityToolkit/dotnet) | [The MIT License](https://opensource.org/licenses/MIT) |
| CoreWCF.WebHttp          | [https://github.com/CoreWCF/CoreWCF](https://github.com/CoreWCF/CoreWCF) | [The MIT License](https://opensource.org/licenses/MIT) |
| CSharpFITS               | [http://vo.iucaa.ernet.in/~voi/CSharpFITS.html](http://vo.iucaa.ernet.in/~voi/CSharpFITS.html) | [The 3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause) |
| CsvHelper                | [https://joshclose.github.io/CsvHelper/](https://joshclose.github.io/CsvHelper/) | [Microsoft Public License (MS-PL)](https://opensource.org/licenses/MS-PL) |
| Dirkster.Avalondock      | [https://github.com/Dirkster99/AvalonDock](https://github.com/Dirkster99/AvalonDock) | [Microsoft Public License (MS-PL)](https://opensource.org/licenses/MS-PL) |
| Fastenshtein             | [https://github.com/DanHarltey/Fastenshtein](https://github.com/DanHarltey/Fastenshtein) | [The MIT License](https://opensource.org/licenses/MIT) |
| Google.Protobuf          | [https://github.com/protocolbuffers/protobuf](https://github.com/protocolbuffers/protobuf) | [The 3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause) |
| Grpc                     | [https://github.com/grpc/grpc-dotnet](https://github.com/grpc/grpc-dotnet) | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) |
| GrpcDotNetNamedPipes     | [https://github.com/cyanfish/grpc-dotnet-namedpipes](https://github.com/cyanfish/grpc-dotnet-namedpipes) | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) |
| Iconic.Zlib.Netstandard  | [https://github.com/HelloKitty/Iconic.Zlib.Netstandard](https://github.com/HelloKitty/Iconic.Zlib.Netstandard) | [zlib License](https://opensource.org/licenses/Zlib) |
| K4os.Compression.LZ4     | [https://github.com/MiloszKrajewski/K4os.Compression.LZ4](https://github.com/MiloszKrajewski/K4os.Compression.LZ4) | [The MIT License](https://opensource.org/licenses/MIT)  |
| MdXaml                   | [https://github.com/whistyun/MdXaml](https://github.com/whistyun/MdXaml) | [The MIT License](https://opensource.org/licenses/MIT) |
| Newtonsoft.Json          | [https://www.newtonsoft.com/json](https://www.newtonsoft.com/json) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| Nito.AsyncEx             | [https://github.com/StephenCleary/AsyncEx](https://github.com/StephenCleary/AsyncEx) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| Nito.Disposables         | [https://github.com/StephenCleary/Disposables](https://github.com/StephenCleary/Disposables) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| Nito.Mvvm.Async          | [https://github.com/StephenCleary/Mvvm](https://github.com/StephenCleary/Mvvm) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| NikonCSWrapper           | [https://sourceforge.net/projects/nikoncswrapper/](https://sourceforge.net/projects/nikoncswrapper/) | [Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/de/legalcode) |
| NJsonSchema              | [https://github.com/RicoSuter/NJsonSchema](https://github.com/RicoSuter/NJsonSchema) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| NOVAS 3.1                | [https://aa.usno.navy.mil/software/novasc_intro](https://aa.usno.navy.mil/software/novasc_intro) | No license requirements |
| OxyPlot                  | [https://oxyplot.github.io/](https://oxyplot.github.io/) | [The MIT License](https://opensource.org/licenses/MIT)          |
| Serilog                  | [https://serilog.net/](https://serilog.net/) | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)          |
| SharpGIS.NmeaParser      | [https://dotmorten.github.io/NmeaParser/](https://dotmorten.github.io/NmeaParser/) | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) |
| Swashbuckle.AspNetCore   | [https://github.com/domaindrivendev/Swashbuckle.AspNetCore](https://github.com/domaindrivendev/Swashbuckle.AspNetCore) | [The MIT License](https://opensource.org/licenses/MIT) |
| SOFA                     | [https://www.iausofa.org/](https://www.iausofa.org/) | [SOFA Software License](https://www.iausofa.org/tandc.html) |
| System.Data.SQLite       | [https://system.data.sqlite.org/](https://system.data.sqlite.org/) | [https://www.sqlite.org/copyright.html](https://www.sqlite.org/copyright.html)
| ToastNotifications       | [https://github.com/raflop/ToastNotifications](https://github.com/raflop/ToastNotifications) | [GNU Lesser General Public License version 3](https://www.gnu.de/documents/lgpl-3.0.de.html) |
| ToggleSwitch             | [https://archive.codeplex.com/?p=toggleswitch](https://archive.codeplex.com/?p=toggleswitch) | [Microsoft Public License (MS-PL)](https://opensource.org/licenses/MS-PL) |
| Trinet.Core.IO.Ntfs      | [https://github.com/RichardD2/NTFS-Streams](https://github.com/RichardD2/NTFS-Streams) | [The 2-Clause BSD License](https://opensource.org/licenses/BSD-2-Clause) |
| VVVV.FreeImage           | [http://freeimage.sourceforge.net/](http://freeimage.sourceforge.net/) | [FreeImage Public License - Version 1.0](http://freeimage.sourceforge.net/freeimage-license.txt) |
| Wpf Extended Toolkit     | [https://github.com/dotnetprojects/WpfExtendedToolkit](https://github.com/dotnetprojects/WpfExtendedToolkit) | [Microsoft Public License (MS-PL)](https://opensource.org/licenses/MS-PL) |



