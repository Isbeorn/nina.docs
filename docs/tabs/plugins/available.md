On the available plugins tab all plugins that have been pushed into the [Plugin Manifest Repository](https://bitbucket.org/Isbeorn/nina.plugin.manifests) will be displayed. (Requires Internet connection). 
For each plugin you will see detailed information about the author, the homepage, what the plugin is about and the source code location of it. Optionally some screenshots can be seen too.  

Furthermore you can manage your plugins here by easily installing the plugins as well as updating those that have a new version.

![Available plugins](../../images/tabs/Plugins_Available.png)

* ![Update](../../images/tabs/plugins/update.png) Installed plugins that can be updated to a newer version are listed at the top. 
* ![Restart](../../images/tabs/plugins/restart.png) Plugins that have been updated or installed will require the application to be restarted. 
* For all available plugins that have neither been updated nor installed no icon is listed next to them. These can be installed by clicking on their list entry and hitting the install button on the top right
* ![Installed](../../images/tabs/plugins/installed.png) Plugins that have already been installed and have no update available will be listed at the bottom 



!!! important
    Plugins are maintained by individual people that may or may not be related to the core N.I.N.A. contributors. When you are facing issues with a plugin, please reach out to the plugin maintainer. In case something is not working properly, try to run N.I.N.A. without plugins to make sure that no plugin is causing these issues.