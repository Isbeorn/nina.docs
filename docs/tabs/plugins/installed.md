In this screen you can manage all plugins that are installed on your machine.
Each plugin shows its Information, Homepage, Author and a description on how to use it.
Furthermore an optional Options area is available where you can customize general options available for this specific plugin

![Installed plugins](../../images/tabs/Plugins_Installed.png)

!!! important
    Plugins are maintained by individual people that may or may not be related to the core N.I.N.A. contributors. When you are facing issues with a plugin, please reach out to the plugin maintainer. In case something is not working properly, try to run N.I.N.A. without plugins to make sure that no plugin is causing these issues.