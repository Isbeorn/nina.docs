The Filter Wheel Tab lets you connect an ASCOM-compatible filter wheel

![Filter_wheel](../../images/tabs/equipments_fw.png)

1. Filter Wheel information
2. Select a filter from the combobox you want to switch to and select the change filter button afterwards
3. List of current filters as imported from ASCOM driver and specified in the [Filter Wheel Options](../options/equipment.md#filterwheel)

