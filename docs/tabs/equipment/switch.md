The Switch Tab lets you connect and control ASCOM-compatible switches. 

![Switch](../../images/tabs/equipment_switches.png)

## Gauges
In this section all readonly switches will be displayed

## Switches
Here all controllable are shown. Switches can be turned on or off as well as switches with a range of values can be set by selecting a target value and then hitting the checkmark button next to it 