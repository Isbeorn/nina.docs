The Safety Monitor lets you connect and control ASCOM-compatible safety monitors. These devices are utilized to report unsafe imaging conditions, like rain or clouds.

![Safety Monitor](../../images/tabs/equipment_safety.png)