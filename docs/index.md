![N.I.N.A. Logo](./images/nina-logo.png)

## About N.I.N.A.

Welcome to N.I.N.A. - Nighttime Imaging 'N' Astronomy. If are reading this document, you have likely [downloaded](//nighttime-imaging.eu/download/) the software - congratulations!

N.I.N.A. is designed for automated DSO imaging using the sequencing model. Whether you are new to the world of DSO imaging or are a seasoned veteran, the goal of N.I.N.A. is to make your imaging sessions easier, faster, and comfortable. N.I.N.A. attempts to make complex concepts and operations easy to understand and effect so that more time is spent on imaging rather than minding equipment.

The N.I.N.A project is also a [FOSS](//en.wikipedia.org/wiki/Free_and_open-source_software) application that is licensed and distributed under the provisions of the [Mozilla Public License Version 2.0](//www.mozilla.org/en-US/MPL/2.0/) license. Its maintenance, maturation, and further development is realized by a team of dedicated volunteers from around the world. If you have an interest in helping the project or contributing to it, please join the project's Discord server (see below) and reading the [contributor guidelines](//bitbucket.org/Isbeorn/nina/src/develop/CONTRIBUTING.md).

## About this Documentation

This document aims to describe the functionality of N.I.N.A. so you can utilize it to its full potential.

!!! tip
    Important notes will appear in boxes similar to this throughout the documentation. If you see one, do not skip over it!

## Getting Help or Helping Out

The [N.I.N.A.'s Discord](//discord.gg/fwpmHU4) chat server is a great way to chat with the creator Stefan Berg the N.I.N.A. contributors and fellow users. Ask questions, share your photos, make suggestions, and help other users. To research past questions, bug reports, or file your own bug report or question, please use the [Issue Tracker](//bitbucket.org/Isbeorn/nina/issues?status=new&status=open).

!!! notice
    Please note that images in this manual might not always reflect what you see as updates and additions to the manual may lag behind the code development process.