N.I.N.A. offers a broad range of capabilities to automate your imaging run.
To do so there are two different approaches available.

## Approach 1   
[The Legacy Sequencer](./simple/simple.md)    
This kind of sequencer is targeted for the more traditional approach like intervalometers with a restricted set of capabilities to offer automation for the most common types of equipment and workflows that will run for a specific number of images. While limited it offers a good starting point for beginners to set up a new sequence and get it running quickly.

## Approach 2  
[The Advanced Sequencer](./advanced/advanced.md)  
Due to the vast range of different equipment and use cases in the astrophotography area, the needs of the legacy sequencer couldn't serve the more specialized use cases. Adding more and more edge cases to the legacy sequencer was no option as that would clog up the user interface for the majority of users that won't even need these special features. Out of this need the advanced sequencer was born. This new approach offers a completely different way of setting up sequences on a fine granular level.  
Instead of restricting the user to a specific workflow, the workflow itself can be customized to specialized requirements with simple building blocks that can be arranged together.