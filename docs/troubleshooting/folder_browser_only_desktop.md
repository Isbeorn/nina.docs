In case you run into the issue where the folder browser only lets you choose from the desktop you can fix it by going through the steps described here:  

[https://www.winhelponline.com/blog/browse-for-folder-dialog-empty-in-windows-11/](https://www.winhelponline.com/blog/browse-for-folder-dialog-empty-in-windows-11/)  


Basically this is caused by a bug in the Windows 11 22H2 build and is fixed in a later patch.  
To get around the problem you need to disable OneDrive sync for the desktop folder.  